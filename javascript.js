let arr = [1, 'text', true, null, 1n, function(){}, {}];

function filterBy(arr, type) {
    return arr.filter(item => typeof item !== type);
}
let filtered = filterBy(arr, "string");
console.log(filtered);